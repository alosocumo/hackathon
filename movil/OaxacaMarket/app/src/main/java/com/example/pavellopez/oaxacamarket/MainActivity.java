package com.example.pavellopez.oaxacamarket;

import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, OnMapReadyCallback {

    GoogleMap map;
    SupportMapFragment mapFragment;
    ArrayList<Punto> listMercados;
    int option = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        //iniciar fragmento
        mapFragment = com.example.pavellopez.oaxacamarket.MapFragment.newInstance();
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.content_map, mapFragment)
                .addToBackStack("Principal")
                .commit();
        mapFragment.getMapAsync(this);


        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.inicio) {

            //Fragment map = new com.example.pavellopez.oaxacamarket.MapFragment();
            setOption(0);
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.content_map, mapFragment)
                    .addToBackStack("Principal")
                    .commit();
            mapFragment.getMapAsync(this);

        }else if(id == R.id.artesania){

            setOption(1);
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.content_map, mapFragment)
                    .addToBackStack("Principal")
                    .commit();
            mapFragment.getMapAsync(this);

        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user gra.nts the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        //Toast.makeText(this,"entro",Toast.LENGTH_SHORT).show();

        map.setMyLocationEnabled(true);
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        Criteria criteria = new Criteria();

        Location location = locationManager.getLastKnownLocation(locationManager.getBestProvider(criteria, false));
        location.getLatitude();
        location.getLongitude();

        LatLng edificioG = new LatLng(location.getLatitude(), location.getLongitude());
        map.addMarker(new MarkerOptions().position(edificioG).title("Yo estoy aquí").icon(BitmapDescriptorFactory.fromResource(R.mipmap.w2)));
        map.moveCamera(CameraUpdateFactory.newLatLng(edificioG));
        CameraPosition zoom = new CameraPosition.Builder().target(edificioG).zoom(13).build();
        map.animateCamera(CameraUpdateFactory.newCameraPosition(zoom));

       Toast.makeText(this,""+getOption(),Toast.LENGTH_SHORT).show();
        if(getOption() == 1){
            LatLng pos = new LatLng(17.0560667,-96.7277805);
            map.addMarker(new MarkerOptions()
                    .position(pos)
                    .title("20 de Noviembre"));
        }



    }

    public int getOption() {
        return option;
    }

    public void setOption(int option) {
        this.option = option;
    }
}







