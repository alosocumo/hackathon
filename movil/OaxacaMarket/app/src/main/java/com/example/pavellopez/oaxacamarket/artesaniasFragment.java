package com.example.pavellopez.oaxacamarket;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;


public class artesaniasFragment extends Fragment {

    List<Punto> listMercados;

    public  artesaniasFragment(){

    }

    public artesaniasFragment(ArrayList<Punto> listMercados){
        this.listMercados = listMercados;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_artesanias, container, false);

        SincronizarArtesanias sincronizarArt = new SincronizarArtesanias();
        sincronizarArt.execute();
        listMercados = sincronizarArt.lista();
        return view;
    }
}
