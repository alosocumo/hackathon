package com.example.pavellopez.oaxacamarket;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Pavel Lopez on 04/05/2017.
 */

public class SincronizarArtesanias extends AsyncTask<Void,String,String> {


    List<Punto> listMercados = new ArrayList<>();

    public SincronizarArtesanias(){
    }


    @Override
    protected String doInBackground(Void... params) {
        // TODO: attempt authentication against a network service.
        StringBuilder result = new StringBuilder();
        try {

            JSONObject contenedorUC = new JSONObject();

            String url = "http://10.42.0.23:8080/hackaton/categoria/obtenerMercadosId/" + 1;
            //String url = "http://172.16.13.156:8080/Comida/alimento/obtenerAlimentos";
            URL direccion = new URL(url);
            HttpURLConnection con = (HttpURLConnection) direccion.openConnection();
            con.setConnectTimeout(5000);
            con.setRequestMethod("GET");
            con.setRequestProperty("Content-Type", "plain/text");
            con.setRequestProperty("Content-Length", contenedorUC.toString().length() + "");
            con.setDoInput(true);
            con.setDoOutput(false);
            con.setReadTimeout(5000);


            int codigo = con.getResponseCode();
            //con esto se puede leer la respuesta de mi conexion
            if (codigo == HttpURLConnection.HTTP_OK) {
                InputStream recibirB = new BufferedInputStream(con.getInputStream());
                BufferedReader leerResultado = new BufferedReader(new InputStreamReader(recibirB));
                String linea;
                while ((linea = leerResultado.readLine()) != null) {
                    result.append(linea);
                }
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


        // TODO: register the new account here.
        return result.toString();
    }

    @Override
    protected void onPostExecute(final String result) {

        if (!result.equals("")) {
            Log.e("Hola", result);
            try {

                JSONArray alimentosRecibidos = new JSONArray(result);

                for (int i = 0; i < alimentosRecibidos.length(); i++) {
                    JSONObject mercoadosDelJSONArray = alimentosRecibidos.getJSONObject(i);
                    Punto punto = new Punto();
                    punto.setNombre(mercoadosDelJSONArray.getString("nombre"));
                    punto.setDescripcion(mercoadosDelJSONArray.getString("que"));
                    punto.setImagen(mercoadosDelJSONArray.getString("url"));
                    punto.setLatitud(mercoadosDelJSONArray.getString("latitud"));
                    punto.setLongitud(mercoadosDelJSONArray.getString("longitud"));

                    listMercados.add(punto);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }

    public List<Punto> lista(){
        return listMercados;
    }


    @Override
    protected void onCancelled() {

    }
}
